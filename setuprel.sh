#!/bin/bash

install-pkg() {
  GITGROUP=$1
  REF=$2
  shift 2
  PKGS="$@"

  mkdir tmp
  cd tmp
  git clone -n https://github.com/$GITGROUP/cmssw.git
  cd cmssw
  git fetch origin
  git checkout $REF $PKGS
  mv * $CMSSW_BASE/src/
  cd ../..
  rm -rf tmp
}

# To figure out which packages a specific repository branch updates with respect to your base workspace:
#  scram p CMSSW CMSSW_{release}
#  cd CMSSW_{release}/src
#  cmsenv
#  git cms-init
#  git diff --name-only HEAD..{last commit of the branch}
#
# You should delete the CMSSW area once done. git cms-init pulls in so many files that are unnecessary.

install-pkg cms-met origin/metTool80X CommonTools/PileupAlgos PhysicsTools/PatAlgos PhysicsTools/PatUtils RecoMET/METProducers
install-pkg cms-met origin/CMSSW_8_0_X-METFilterUpdate RecoMET/METFilters
#install-pkg cms-met origin/fromCMSSW_8_0_20_postICHEPfilter RecoMET/METFilters
install-pkg emanueledimarco origin/ecal_smear_fix_80X EgammaAnalysis/ElectronTools
git clone -b ICHEP2016_v2 https://github.com/ECALELFS/ScalesSmearings.git EgammaAnalysis/ElectronTools/data/ScalesSmearings
